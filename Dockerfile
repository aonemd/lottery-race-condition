FROM golang:1.19 as build

WORKDIR /lottery

# cache go modules
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o lottery

# Production stage
FROM alpine:3.12.1 as runtime

WORKDIR /service

COPY --from=build /lottery .

EXPOSE 6666

ENTRYPOINT ["/service/lottery"]
