package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/go-redis/redis"
)

func redisClient(port string) *redis.Client {
  var redis_host string
  if port == "" {
    redis_host = "redis:6379"
  } else {
    redis_host = fmt.Sprintf("redis:%s", port)
  }

  redis_password := ""
  redis_db := 0

  opts := &redis.Options{
    Addr:     redis_host,
    Password: redis_password,
    DB:       int(redis_db),
  }

  rc := redis.NewClient(opts)

  // fmt.Println(rc.SetNX("TestInstanceConnection", 1, 4 * time.Second).Val())
  // resp, err := rc.Ping().Result()
  // if err != nil {
  //   panic("ERROR: Connecting to Redis")
  // }
  // fmt.Println(resp)

  return rc
}

var clients []*redis.Client = []*redis.Client{
  redisClient("6377"),
  redisClient("6378"),
  redisClient("6379"),
}
var client *redis.Client = clients[0]

type User struct {
  name rune
  tickets []*Ticket
}

func (u *User) draw(opId string) {
  for i, ticket := range u.tickets {
    if !ticket.taken {
      ticket.draw(fmt.Sprintf("%s %d %c", opId, i, u.name))
      break
    }
  }
}


func (u *User) drawNoRC(opId string) {
	defer wg.Done()

  // hitting the DB
  for _, ticket := range u.tickets {
    if !ticket.taken {
      client.LPush("ops", ticket.id)
    }
  }


  // consuming
  for {
    tId := client.RPop("ops").Val()
    if tId == "" {
      // if the list is empty
      break
    }

    // committing DB transaction
    id, _ := strconv.ParseInt(tId, 10, 64)
    t := u.findTicketById(id)
    t.draw(fmt.Sprintf("%s tId: %d %c", opId, id, u.name))
  }
}

func (u *User) drawNoRCSetnx(opId string) {
	defer wg.Done()

  // hitting the DB
  for i, ticket := range u.tickets {
    if !ticket.taken {
      var lockName = fmt.Sprintf("lock-%d", ticket.id)

      // 1 second is enough here for the transaction to be committed
      if client.SetNX(lockName, 1, 1 * time.Second).Val() == true {
        // new lock
        fmt.Println("new lock:", lockName)

        ticket.draw(fmt.Sprintf("%s %d %c", opId, i, u.name))

        // // the automatic deletion after processing does not work; looks like
        // // there's not enough time to update the ticket state, the other client
        // // acquires the same ticket/lock and tries the already processed ticket
        // // so setting a long duration on the lock works here...
        // fmt.Println(client.Del(lockName))

        break
      } else {
        // lock exists! Find another one...
        fmt.Println("existing lock:", lockName)

        continue
      }
    }
  }
}

func (u *User) drawNoRCRedlock(opId string) {
	defer wg.Done()

  consensusRequiredCount := len(clients)/2 + 1 // (N/2 + 1); N: number of Redis instances

  for i, ticket := range u.tickets {
    if !ticket.taken {
      var lockName = fmt.Sprintf("lock-%d", ticket.id)

      if !redlockLock(lockName, 5*time.Second, 2*time.Second, consensusRequiredCount) {
        continue
      }
      fmt.Println("locked!")

      ticket.draw(fmt.Sprintf("%s %d %c", opId, i, u.name))
      fmt.Println("drawn!")

      if !redlockUnlock(lockName, consensusRequiredCount) {
        continue
      }
      fmt.Println("unlocked!")

      break
    }
  }
}

func redlockLock(guid string, expiration time.Duration, drift time.Duration, consensusRequiredCount int) bool {
  consensusAquiredCount := 0

  for _, rc := range clients {
    start := time.Now()

    res := rc.SetNX(guid, 1, time.Duration(expiration)).Val()
    fmt.Println("lock result: ", res)
    // if rc.SetNX(guid, 1, time.Duration(expiration.Milliseconds())).Val() == true {
    if res == true {
      now := time.Now()

      isTimeValid := (expiration - (now.Sub(start) - drift)) > 0

      if isTimeValid {
        consensusAquiredCount += 1
      }
    }
  }

  if consensusAquiredCount < consensusRequiredCount {
    // did not get consensus, unlock
    fmt.Println("did not get consensus, unlock")
    redlockUnlock(guid, consensusRequiredCount)
    return false
  }

  return true
}

func redlockUnlock(guid string, consensusRequiredCount int) bool {
  consensusAquiredCount := 0

  for _, rc := range clients {

    if rc.Del(guid).Val() == 1 {
      consensusAquiredCount += 1
    }
  }

  if consensusAquiredCount < consensusRequiredCount {
    // error: not all locks were freed!
    fmt.Println("error: not all locks were freed!")
    return false
  }

  return true
}

func (u User) findTicketById(id int64) *Ticket {
  for _, t := range u.tickets {
    if t.id == id {
      return t
    }
  }

  return nil
}

func (u User) String() string {
  return fmt.Sprintf("{ name: %c, tickets: %v }", u.name, u.tickets)
}

type Ticket struct {
	id    int64
	user  rune
	taken bool
}

func NewTicket(id int64, user rune, taken bool) *Ticket {
	return &Ticket{
		id:    id,
		user:  user,
		taken: taken,
	}
}

func (t *Ticket) draw(opId string) {
  fmt.Println("processing operation:", opId)

	if t.taken {
    fmt.Println("Users:", users)
		err := fmt.Sprintf("ERR: operation '%s' failed. Ticket is TAKEN already!", opId)
		panic(err)
	}

	t.taken = true
}

func (t Ticket) String() string {
	return fmt.Sprintf("{ id: %d, user: '%c', taken: %t }", t.id, t.user, t.taken)
}

type Winning struct {
	ticketId int64
	reward   float64
}

var wg sync.WaitGroup

var users = []User {
  User {
    name: 'A',
    tickets: []*Ticket {
      NewTicket(1, 'A', true),
      NewTicket(3, 'A', false),
      NewTicket(4, 'A', false),
    },
  },
  User {
    name: 'B',
    tickets: []*Ticket {
      NewTicket(2, 'B', false),
    },
  },
}

var winnings = []Winning{}

func main() {
  // USAGE: `docker-compose up --build`
  // check the logs

	wg.Add(2)
  // // test with these first; sometimes they will race to modify the first ticket
  // go users[0].draw("first")
  // go users[0].draw("second")

  // // then test with these; they will modify two tickets and sometimes they will race to modify a 3rd
  // fmt.Println("USING LPUSH/RPOP; a queue")
  // go users[0].drawNoRC("first")
  // go users[0].drawNoRC("second")

  // fmt.Println("USING SETNX")
  // go users[0].drawNoRCSetnx("first")
  // go users[0].drawNoRCSetnx("second")

  // fmt.Println("Redis Redlock")
  go users[0].drawNoRCRedlock("first")
  go users[0].drawNoRCRedlock("second")

	wg.Wait()

  fmt.Println("Users:", users)
}
