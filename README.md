### Resources
- https://dev.to/lazypro/redis-helps-avoiding-racing-conditions-44g2
- https://redis.io/commands/setnx/
- https://redis.io/docs/manual/patterns/distributed-locks/
- https://dev.to/lazypro/explain-redlock-in-depth-31jj
- https://levelup.gitconnected.com/implementing-redlock-on-redis-for-distributed-locks-a3cfe60d4ea4
- https://klaviyo.tech/using-redis-one-way-gates-to-eliminate-massively-parallel-high-speed-race-conditions-be1228e1c488
